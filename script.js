// Завдання 1: Створення нового елементу <a> та його додавання
const footer = document.getElementById('footer');
const learnMoreLink = document.createElement('a');
learnMoreLink.textContent = 'Learn More';
learnMoreLink.href = '#';
footer.appendChild(learnMoreLink);

// Завдання 2: Створення нового елементу <select> та додавання його до головного блоку 
const main = document.querySelector('main');
const selectElement = document.createElement('select');
selectElement.id = 'rating';

// Завдання 2 (продовження): Створення нових елементів <option> та їх додавання до елементу <select>
const ratings = [
  { value: '4', text: '4 Зірка' },
  { value: '3', text: '3 Зірка' },
  { value: '2', text: '2 Зірка' },
  { value: '1', text: '1 Зірка' }
];

ratings.forEach(rating => {
  const option = document.createElement('option');
  option.value = rating.value;
  option.textContent = rating.text;
  selectElement.appendChild(option);
});

main.insertBefore(selectElement, main.querySelector('.features'));
